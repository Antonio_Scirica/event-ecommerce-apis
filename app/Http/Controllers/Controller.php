<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function success($data, $statusCode = 200)
    {
      $response = [
        'error' => null,
        'data' => $data
      ];

      return response($response, $statusCode);
    }

}
