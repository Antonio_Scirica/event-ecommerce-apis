<?php

namespace App\Http\Controllers;



use App\Models\Event;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index()
    {
      $events = Event::all();
      return $this->success($events);
    }

    public function show($id)
    {
      $event = Event::find($id);
      return $this->success($event);
    }

    public function create(Request $request)
    {
      $this->validate($request, Event::VALIDATION);
      $event = Event::create($request->all());
      return $this->success($event, 201);
    }

    public function update(Request $request, $id)
    {
      $event = Event::find($id);
      $this->validate($request, Event::VALIDATION_UPDATE);
      $event->update($request->all());
      return $this->success($event);
    }
}
