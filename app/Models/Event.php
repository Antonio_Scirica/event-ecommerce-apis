<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    const VALIDATION = [
      'nome' => 'required|string',
      'prezzo' => 'required|numeric',
      'indirizzo' => 'required|string',
      'lat' => 'required|numeric',
      'lng' => 'required|numeric'
    ];

    const VALIDATION_UPDATE = [
      'nome' => 'string',
      'prezzo' => 'numeric',
      'indirizzo' => 'string',
      'lat' => 'numeric',
      'lng' => 'numeric'
    ];



    protected $fillable = [
      'nome',
      'prezzo',
      'indirizzo',
      'lat',
      'lng'
    ];

    protected $attributes = [
      'numero_visualizzazioni' => 0,
      'numero_commenti' => 0,
      'numero_like' => 0
    ];
}



 ?>
